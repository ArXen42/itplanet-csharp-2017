﻿using System.IO;
using HoMM.ClientClasses;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

namespace HoMM.Client.Tests
{
	public static class TestsHelpers
	{
		public static HommSensorData GetLevel2DebugMapSensorData()
		{
			HommSensorData sensorData;
			using (var streamReader = new StreamReader(TestContext.CurrentContext.TestDirectory + "/TestsData/level2-debugmap-sensor-data.json"))
			{
				JObject obj = JObject.Load(new JsonTextReader(streamReader));
				sensorData = obj.ToObject<HommSensorData>();
			}

			return sensorData;
		}
	}
}