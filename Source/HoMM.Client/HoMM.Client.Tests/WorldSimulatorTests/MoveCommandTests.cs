﻿using HoMM.Client.Simulation;
using HoMM.Client.Simulation.Commands;
using NUnit.Framework;

namespace HoMM.Client.Tests.WorldSimulatorTests
{
	[TestFixture]
	public class MoveCommandTests
	{
		[Test]
		public void SimpleRollbackDoesNothing()
		{
			var data = TestsHelpers.GetLevel2DebugMapSensorData();
			var sim = new WorldSimulator(data);

			var command = new MoveCommand(sim, data.MyRespawnSide, Direction.Down);
			CommonValidations.AssertCommandRollbacksCorrectly(command);
		}

		[Test]
		public void RollbackWithResourceCollectionCorrect()
		{
			var data = TestsHelpers.GetLevel2DebugMapSensorData();
			var sim = new WorldSimulator(data);

			sim.ApplyCommand(new MoveCommand(sim, data.MyRespawnSide, Direction.Down));

			var command = new MoveCommand(sim, data.MyRespawnSide, Direction.Down);
			CommonValidations.AssertCommandRollbacksCorrectly(command);
		}

		[Test]
		public void MovementTimeCalculatesCorrectly()
		{
			var data = TestsHelpers.GetLevel2DebugMapSensorData();
			var sim = new WorldSimulator(data);

			sim.ApplyCommand(new MoveCommand(sim, data.MyRespawnSide, Direction.Down));
			sim.ApplyCommand(new MoveCommand(sim, data.MyRespawnSide, Direction.Down));
			sim.ApplyCommand(new MoveCommand(sim, data.MyRespawnSide, Direction.Down));

			Assert.AreEqual(sim.GetLatestCommand().EndTime - sim.InitialTime, 0.75*0.5*3, 0.02);
		}
	}
}