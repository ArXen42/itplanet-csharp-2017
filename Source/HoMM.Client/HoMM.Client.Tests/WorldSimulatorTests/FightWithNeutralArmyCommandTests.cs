﻿using HoMM.Client.Simulation;
using HoMM.Client.Simulation.Commands;
using NUnit.Framework;
using Shouldly;

namespace HoMM.Client.Tests.WorldSimulatorTests
{
	[TestFixture]
	public class FightWithNeutralArmySimulatorCommandTests
	{
		[Test]
		public void RollbackDeathCorrect()
		{
			var data = TestsHelpers.GetLevel2DebugMapSensorData();
			var sim = new WorldSimulator(data);

			CommonValidations.AssertCommandRollbacksCorrectly(
				new MoveCommand(sim, data.MyRespawnSide, Direction.RightDown));
		}

		public void RollbackWithoutDeathCorrect()
		{
			var data = TestsHelpers.GetLevel2DebugMapSensorData();
			var sim = new WorldSimulator(data);

			sim.ApplyCommand(new MoveCommand(sim, data.MyRespawnSide, Direction.Down));
			sim.ApplyCommand(new MoveCommand(sim, data.MyRespawnSide, Direction.Down));
			sim.ApplyCommand(new MoveCommand(sim, data.MyRespawnSide, Direction.Down));
			sim.ApplyCommand(new MoveCommand(sim, data.MyRespawnSide, Direction.Down));
			sim.ApplyCommand(new MoveCommand(sim, data.MyRespawnSide, Direction.RightUp));
			sim.ApplyCommand(new MoveCommand(sim, data.MyRespawnSide, Direction.RightDown));
			sim.ApplyCommand(new HireUnitsCommand(sim, data.MyRespawnSide, 42));
			sim.ApplyCommand(new MoveCommand(sim, data.MyRespawnSide, Direction.Up));
			sim.ApplyCommand(new MoveCommand(sim, data.MyRespawnSide, Direction.Up));
			sim.ApplyCommand(new MoveCommand(sim, data.MyRespawnSide, Direction.Up));


			CommonValidations.AssertCommandRollbacksCorrectly(
				new MoveCommand(sim, data.MyRespawnSide, Direction.Up));
		}

		[Test]
		public void RespawnCorrect()
		{
			var data = TestsHelpers.GetLevel2DebugMapSensorData();
			var sim = new WorldSimulator(data);

			sim.ApplyCommand(new MoveCommand(sim, data.MyRespawnSide, Direction.RightDown));

			var heroLocation = sim.GetPlayerNode(data.MyRespawnSide).MapObjectData.Location;
			heroLocation.X.ShouldBe(0);
			heroLocation.Y.ShouldBe(0);
		}
	}
}