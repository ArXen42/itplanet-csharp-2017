﻿using System;
using HoMM.Client.Simulation;
using NUnit.Framework;

namespace HoMM.Client.Tests.WorldSimulatorTests
{
	[TestFixture]
	public class PeriodicEventsTests
	{
		[TestCase(0, 1, ExpectedResult = false)]
		[TestCase(0, 2, ExpectedResult = false)]
		[TestCase(0, 3, ExpectedResult = false)]
		[TestCase(0, 5, ExpectedResult = true)]
		[TestCase(0, 6, ExpectedResult = true)]
		[TestCase(1.1, 2.4, ExpectedResult = false)]
		[TestCase(1.1, 6.3, ExpectedResult = true)]
		[TestCase(5.0, 9.2, ExpectedResult = false)]
		[TestCase(5.0, 10, ExpectedResult = true)]
		public Boolean DailyChecksCorrect(Double startTime, Double endTime)
		{
			return WorldSimulator.IsPeriodPassed(startTime, endTime, 5.0);
		}
	}
}