﻿using System;
using System.Collections.Generic;
using System.Linq;
using HoMM.Client.Simulation;
using HoMM.Client.Simulation.Commands;
using HoMM.ClientClasses;
using Shouldly;

namespace HoMM.Client.Tests.WorldSimulatorTests
{
	public static class CommonValidations
	{
		public static void AssertCommandRollbacksCorrectly(SimulatorCommand commandToTest)
		{
			var sim = commandToTest.Simulator;
			AssertActionDoesNoChangs(sim,
				() =>
				{
					sim.ApplyCommand(commandToTest);
					sim.RollbackLastCommand();
				});
		}

		public static void AssertActionDoesNoChangs(WorldSimulator sim, Action action)
		{
			String initialArmy1 = GetArmyString(sim.Players["Left"].Army);
			String initialArmy2 = GetArmyString(sim.Players["Right"].Army);

			String initialTreasury1 = GetTreasuryString(sim.Players["Left"].Treasury);
			String initialTreasury2 = GetTreasuryString(sim.Players["Right"].Treasury);

			String initialScores = $"{sim.Players["Left"].Scores} {sim.Players["Right"].Scores}";

			String initialPos1 = sim.GetPlayerNode("Left").MapObjectData.Location.ToString();
			String initialPos2 = sim.GetPlayerNode("Right").MapObjectData.Location.ToString();

			String initialMap = GetMapString(sim);

			var initialUnitsInDwellings = GetDwellingsSum(sim);

			action.Invoke();

			String army1 = GetArmyString(sim.Players["Left"].Army);
			String army2 = GetArmyString(sim.Players["Right"].Army);

			String treasury1 = GetTreasuryString(sim.Players["Left"].Treasury);
			String treasury2 = GetTreasuryString(sim.Players["Right"].Treasury);

			String scores = $"{sim.Players["Left"].Scores} {sim.Players["Right"].Scores}";

			String pos1 = sim.GetPlayerNode("Left").MapObjectData.Location.ToString();
			String pos2 = sim.GetPlayerNode("Right").MapObjectData.Location.ToString();

			String map = GetMapString(sim);

			var unitsInDwellings = GetDwellingsSum(sim);

			army1.ShouldBe(initialArmy1);
			army2.ShouldBe(initialArmy2);

			treasury1.ShouldBe(initialTreasury1);
			treasury2.ShouldBe(initialTreasury2);

			scores.ShouldBe(initialScores);

			pos1.ShouldBe(initialPos1);
			pos2.ShouldBe(initialPos2);

			initialMap.ShouldBe(map);

			foreach (var unit in unitsInDwellings)
				initialUnitsInDwellings[unit.Key].ShouldBe(unit.Value);
		}

		private static String GetArmyString(Dictionary<UnitType, Int32> army)
		{
			return army.Select(z => z.Value + " " + z.Key).Aggregate((a, b) => a + ", " + b);
		}

		private static String GetTreasuryString(Dictionary<Resource, Int32> treasury)
		{
			return treasury.Select(z => z.Value + " " + z.Key).Aggregate((a, b) => a + ", " + b);
		}

		private static String GetMapString(WorldSimulator sim)
		{
			return sim.Map.Vertices.Select(v => GetMapObjectDataString(v.MapObjectData)).Aggregate((a, b) => a + ", " + b);
		}

		private static String GetMapObjectDataString(MapObjectData mapObjectData)
		{
			String str = mapObjectData.Terrain.ToString();
			if (mapObjectData.Wall != null)
				throw new ArgumentException("Wall? In simulator?");
			if (mapObjectData.Garrison != null)
				str += $" Garrison with {GetArmyString(mapObjectData.Garrison.Army)}";
			if (mapObjectData.NeutralArmy != null)
				str += $" Neutral army with {GetArmyString(mapObjectData.NeutralArmy.Army)}";
			if (mapObjectData.Mine != null)
				str += $" Mine of {mapObjectData.Mine.Resource}";
			if (mapObjectData.Dwelling != null)
				str += $" Dwelling of {mapObjectData.Dwelling.UnitType}";
			if (mapObjectData.ResourcePile != null)
				str += $" Resource pile of {mapObjectData.ResourcePile.Amount} {mapObjectData.ResourcePile.Resource}";
			if (mapObjectData.Hero != null)
				str += $"{mapObjectData.Hero.Name} Hero";

			return str;
		}

		private static Dictionary<UnitType, Int32> GetDwellingsSum(WorldSimulator sim)
		{
			var result = new Dictionary<UnitType, Int32>()
			{
				{UnitType.Cavalry, 0},
				{UnitType.Infantry, 0},
				{UnitType.Militia, 0},
				{UnitType.Ranged, 0}
			};

			foreach (var node in sim.Dwellings)
			{
				var dwelling = node.MapObjectData.Dwelling;
				result[dwelling.UnitType] += dwelling.AvailableToBuyCount;
			}

			return result;
		}
	}
}