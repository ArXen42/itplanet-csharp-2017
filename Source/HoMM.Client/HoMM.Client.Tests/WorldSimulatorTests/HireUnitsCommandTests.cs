﻿using HoMM.Client.Simulation;
using HoMM.Client.Simulation.Commands;
using NUnit.Framework;

namespace HoMM.Client.Tests.WorldSimulatorTests
{
	[TestFixture]
	public class HireUnitsCommandTests
	{
		[Test]
		public void RollbackCorrect()
		{
			var data = TestsHelpers.GetLevel2DebugMapSensorData();
			var sim = new WorldSimulator(data);

			sim.ApplyCommand(new MoveCommand(sim, data.MyRespawnSide, Direction.Down));
			sim.ApplyCommand(new MoveCommand(sim, data.MyRespawnSide, Direction.Down));
			sim.ApplyCommand(new MoveCommand(sim, data.MyRespawnSide, Direction.Down));
			sim.ApplyCommand(new MoveCommand(sim, data.MyRespawnSide, Direction.Down));
			sim.ApplyCommand(new MoveCommand(sim, data.MyRespawnSide, Direction.RightUp));
			sim.ApplyCommand(new MoveCommand(sim, data.MyRespawnSide, Direction.RightDown));

			CommonValidations.AssertCommandRollbacksCorrectly(new HireUnitsCommand(sim, data.MyRespawnSide, 20));
		}
	}
}