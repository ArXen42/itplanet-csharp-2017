﻿using System;
using HoMM.Client.Simulation;
using HoMM.Client.Simulation.Commands;
using NUnit.Framework;
using NUnit.Framework.Internal;

namespace HoMM.Client.Tests.WorldSimulatorTests
{
	[TestFixture()]
	public class CommandsChainTests
	{
		[Test]
		public void Test()
		{
			var data = TestsHelpers.GetLevel2DebugMapSensorData();
			var sim = new WorldSimulator(data);

			Action action = () =>
			{
				for (Int32 i = 0; i < 7; i++)
					sim.ApplyCommand(new MoveCommand(sim, data.MyRespawnSide, Direction.Down));

				sim.ApplyCommand(new MoveCommand(sim, data.MyRespawnSide, Direction.RightDown));

				for (Int32 i = 0; i < 4; i++)
					sim.ApplyCommand(new MoveCommand(sim, data.MyRespawnSide, Direction.Up));

				sim.ApplyCommand(new MoveCommand(sim, data.MyRespawnSide, Direction.RightDown));
				sim.ApplyCommand(new HireUnitsCommand(sim, data.MyRespawnSide, 120));
				sim.ApplyCommand(new MoveCommand(sim, data.MyRespawnSide, Direction.RightDown));
				sim.ApplyCommand(new HireUnitsCommand(sim, data.MyRespawnSide, 120));

				sim.ApplyCommand(new MoveCommand(sim, data.MyRespawnSide, Direction.Up));
				sim.ApplyCommand(new MoveCommand(sim, data.MyRespawnSide, Direction.Up));
				sim.ApplyCommand(new MoveCommand(sim, data.MyRespawnSide, Direction.Up));
				sim.ApplyCommand(new MoveCommand(sim, data.MyRespawnSide, Direction.Up));

				while (sim.GetLatestCommand() != null)
				{
					sim.RollbackLastCommand();
				}
			};

			CommonValidations.AssertActionDoesNoChangs(sim, action);
		}
	}
}