﻿using System;
using HoMM.Client.Simulation;
using HoMM.ClientClasses;

namespace HoMM.Client.AI
{
	public sealed class MovesAnalizer
	{
		public MovesAnalizer(HommSensorData sensorData, Double upperTimeLimit, Int64 maxMilliseconds, MapObjectData unreachableSite = null)
		{
			WorldSimulator = new WorldSimulator(sensorData, unreachableSite);

			UpperTimeLimit = upperTimeLimit;

			_playerSide = sensorData.MyRespawnSide;
			_player = WorldSimulator.Players[_playerSide];

			var enemyRespawnSite = _playerSide == "Left" ? WorldSimulator.Map.RightRespawnSite : WorldSimulator.Map.LeftRespawnSite;
			if (enemyRespawnSite != null)
				enemyRespawnSite.Reachable = false;
			// Seems like we won't simulate enemy player anyway, so there is no problem with marking his respawn as unreachable (crutch)
		}

		public readonly Double UpperTimeLimit;
		public readonly WorldSimulator WorldSimulator;

		public SequencesBlock GetBestCommands()
		{
			throw new NotImplementedException();
		}

		private readonly String _playerSide;
		private readonly PlayerData _player;
	}
}