﻿using System;
using System.Collections.Generic;
using HoMM.Client.Simulation.Commands;
using HoMM.ClientClasses;

namespace HoMM.Client.AI
{
	public sealed class AIController
	{
		public AIController(HommSensorData sensorData)
		{
			Update(sensorData);
		}

		public readonly Double GameLength = 90;

		public Int32 ObjectsOnMapCount => _sensorData.Map.Width*_sensorData.Map.Height;
		public Int32 RevealedObjectsCount => _sensorData.Map.Objects.Count;
		public Int32 ObjectsLeftToRevealCount => ObjectsOnMapCount - RevealedObjectsCount;
		public HommSensorData SensorData => _sensorData;
		public Boolean IsPendingRecalculate => _isPendingRecalculate;

		public void Update(HommSensorData recentSensorData, LocationInfo unreachableSiteLocation = null)
		{
			if (_sensorData != null)
			{
				foreach (var recentMapObjectData in recentSensorData.Map.Objects)
				{
					var oldObject = Helpers.GetObjectAt(_sensorData.Map, recentMapObjectData.Location);

					if (oldObject != null)
					{
						_sensorData.Map.Objects.Remove(oldObject);

						if (oldObject.Mine != null
						    && oldObject.Mine.Owner == _sensorData.MyRespawnSide && recentMapObjectData.Mine.Owner != _sensorData.MyRespawnSide)
						{
							_isPendingRecalculate = true;
						}
					}

					_sensorData.Map.Objects.Add(recentMapObjectData);
				}

				recentSensorData.Map = _sensorData.Map;
			}

			_sensorData = recentSensorData;
			_unreachableSite = unreachableSiteLocation == null
				? null
				: Helpers.GetObjectAt(_sensorData.Map, unreachableSiteLocation);
		}

		public CommandsSequence GetBestSequence()
		{
			throw new NotImplementedException();
		}


		private HommSensorData _sensorData;
		private Boolean _isPendingRecalculate;

		private MapObjectData _unreachableSite;
	}

	public sealed class SequencesBlock : List<CommandsSequence>
	{
		public SequencesBlock() { }
		public SequencesBlock(Int32 capacity) : base(capacity) { }
		public SequencesBlock(IEnumerable<CommandsSequence> collection) : base(collection) { }

		public Double EndTime => this[0].EndTime;
		public Int32 Scores => this[0].Scores;
	}

	public sealed class CommandsSequence : List<SimulatorCommand>
	{
		public CommandsSequence() { }
		public CommandsSequence(Int32 capacity) : base(capacity) { }
		public CommandsSequence(IEnumerable<SimulatorCommand> collection) : base(collection) { }

		public Double EndTime => this[Count - 1].EndTime;
		public Int32 Scores => this[Count - 1].Scores;
	}
}