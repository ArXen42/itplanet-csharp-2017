﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using HoMM.Client.Simulation.Commands;
using HoMM.ClientClasses;
using Shouldly;

namespace HoMM.Client.Simulation
{
	public sealed class WorldSimulator
	{
		public WorldSimulator(HommSensorData sensorData, MapObjectData unreachableSite = null)
		{
			SensorData = sensorData;
			InitialTime = sensorData.WorldCurrentTime;

			Players = new ReadOnlyDictionary<String, PlayerData>(new Dictionary<String, PlayerData>()
			{
				{"Left", new PlayerData(sensorData, "Left")},
				{"Right", new PlayerData(sensorData, "Right")}
			});

			Map = new Map(sensorData.Map);

			Dwellings = Map.Vertices.Where(v => v.MapObjectData.Dwelling != null);
			_beforeGrowsCapacities = new Dictionary<ClientClasses.Dwelling, Int32>();
			foreach (var node in Dwellings)
				_beforeGrowsCapacities.Add(node.MapObjectData.Dwelling, 0);

			Mines = Map.Vertices.Where(v => v.MapObjectData.Mine != null);

			LeftCommands = new Stack<SimulatorCommand>();
			RightCommands = new Stack<SimulatorCommand>();
			Commands = new ReadOnlyDictionary<String, Stack<SimulatorCommand>>(new Dictionary<String, Stack<SimulatorCommand>>
			{
				{"Left", LeftCommands},
				{"Right", RightCommands}
			});

			if (unreachableSite != null)
				Map.Vertices.First(v => v.MapObjectData == unreachableSite).Reachable = false;
		}

		public readonly HommSensorData SensorData;

		public readonly Map Map;

		public readonly IEnumerable<SimulatorMapNode> Dwellings;

		public readonly IEnumerable<SimulatorMapNode> Mines;

		public readonly Double InitialTime;

		public readonly ReadOnlyDictionary<String, PlayerData> Players;

		public readonly ReadOnlyDictionary<String, Stack<SimulatorCommand>> Commands;

		public readonly Stack<SimulatorCommand> LeftCommands, RightCommands;

		public Stack<SimulatorCommand> GetLatestTimeStack()
		{
			Double leftEndTime = LeftCommands.FirstOrDefault()?.EndTime ?? InitialTime;
			Double rightEndTime = RightCommands.FirstOrDefault()?.EndTime ?? InitialTime;

			return leftEndTime > rightEndTime ? LeftCommands : RightCommands;
		}

		public SimulatorCommand GetLatestCommand() => GetLatestTimeStack().FirstOrDefault();

		public Double GetLatestCommandTime() => GetLatestCommand()?.EndTime ?? InitialTime;

		public Double GetLatestCommandTime(String side) => Commands[side].FirstOrDefault()?.EndTime ?? InitialTime;

		public void ApplyCommand(SimulatorCommand command)
		{
			while (command != null)
			{
				String playerSide = command.Side;

				command.Apply();
				Commands[playerSide].Push(command);
				ApplyDailyEvents(command);
				ApplyWeeklyEvents(command);

#if DEBUG //for the sake of perfomance
				command.Simulator.ShouldBe(this);
				String oppositeSide = Helpers.GetOppositeSide(playerSide);
				Double oppositeEndTime = GetLatestCommandTime(oppositeSide);
				command.EndTime.ShouldBeGreaterThan(oppositeEndTime, "Tried to insert command into past.");
#endif
				command = command.ChainedCommand;
			}
		}

		public SimulatorCommand RollbackLastCommand()
		{
			while (true)
			{
				var command = GetLatestTimeStack().Pop();
				RollbackWeeklyEvents(command);
				RollbackDailyEvents(command);
				command.Rollback();
				command.ChainedCommand = null;

				if (GetLatestCommand()?.ChainedCommand != command) return command;
			}
		}

		public SimulatorMapNode GetPlayerNode(String side)
		{
			return Map.Vertices.FirstOrDefault(n => n.MapObjectData.Hero != null && n.MapObjectData.Hero.Name == side);
		}

		private void ApplyDailyEvents(SimulatorCommand appliedCommand)
		{
			if (IsPeriodPassed(appliedCommand.StartTime, appliedCommand.EndTime, HommRules.Current.DailyTickInterval))
			{
				foreach (var player in Players.Values)
				foreach (var mine in player.OwnedMines)
				{
					player.Treasury[mine.Resource] += HommRules.Current.MineDailyResourceYield;
					player.Scores += HommRules.Current.MineOwningDailyScores;
				}
			}
		}

		private void ApplyWeeklyEvents(SimulatorCommand appliedCommand)
		{
			if (IsPeriodPassed(appliedCommand.StartTime, appliedCommand.EndTime, HommRules.Current.DailyTickInterval*7))
			{
				foreach (var dwellingNode in Dwellings)
				{
					var dwelling = dwellingNode.MapObjectData.Dwelling;
					_beforeGrowsCapacities[dwelling] = dwelling.AvailableToBuyCount;
					dwelling.AvailableToBuyCount = Math.Min(
						UnitsConstants.Current.WeeklyGrowth[dwelling.UnitType] + dwelling.AvailableToBuyCount,
						HommRules.Current.DwellingCapacity);
				}
			}
		}

		private void RollbackWeeklyEvents(SimulatorCommand appliedCommand)
		{
			if (IsPeriodPassed(appliedCommand.StartTime, appliedCommand.EndTime, HommRules.Current.DailyTickInterval*7))
			{
				foreach (var dwellingNode in Dwellings)
				{
					var dwelling = dwellingNode.MapObjectData.Dwelling;

					dwelling.AvailableToBuyCount = _beforeGrowsCapacities[dwelling]; //Won't work if there is more then two weeks in the game
				}
			}
		}

		private void RollbackDailyEvents(SimulatorCommand rollbackedCommand)
		{
			if (IsPeriodPassed(rollbackedCommand.StartTime, rollbackedCommand.EndTime, HommRules.Current.DailyTickInterval))
			{
				foreach (var player in Players.Values)
				foreach (var mine in player.OwnedMines)
				{
					player.Treasury[mine.Resource] -= HommRules.Current.MineDailyResourceYield;
					player.Scores -= HommRules.Current.MineOwningDailyScores;
				}
			}
		}

		public static Boolean IsPeriodPassed(Double startTime, Double endTime, Double interval)
		{
			(endTime - startTime).ShouldBeLessThan(interval*2);

			Int32 flooredStart = (Int32) (startTime/interval);
			Int32 flooredEnd = (Int32) (endTime/interval);

			Int32 delta = flooredEnd - flooredStart;
			return delta > 0;
		}

		private readonly Dictionary<ClientClasses.Dwelling, Int32> _beforeGrowsCapacities;
	}
}