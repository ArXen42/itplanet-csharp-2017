﻿using QuickGraph;

namespace HoMM.Client.Simulation
{
	public sealed class SimulatorMapEdge : UndirectedEdge<SimulatorMapNode>
	{
		public SimulatorMapEdge(SimulatorMapNode source, SimulatorMapNode target, Direction direction) : base(source, target)
		{
			SourceTargetDirection = direction;
			TargetSourceDirection = Helpers.GetOppositeDirection(direction);
		}

		public readonly Direction SourceTargetDirection;

		public readonly Direction TargetSourceDirection;
	}
}