﻿using System;
using System.Collections.Generic;
using System.Linq;
using HoMM.ClientClasses;

namespace HoMM.Client.Simulation
{
	public class PlayerData
	{
		public PlayerData(HommSensorData sensorData, String side)
		{
			if (sensorData.MyRespawnSide == side)
			{
				Army = sensorData.MyArmy;
				Treasury = sensorData.MyTreasury;
			}
			else
			{
				MapObjectData enemyPlayerData = sensorData.Map.Objects.FirstOrDefault(
					o => o.Hero != null && o.Hero.Name == side);

				if (enemyPlayerData != null)
					Army = enemyPlayerData.Hero.Army;

				Treasury = new Dictionary<Resource, Int32> //Unknown
				{
					{Resource.Ebony, 0},
					{Resource.Glass, 0},
					{Resource.Gold, 0},
					{Resource.Iron, 0}
				};
			}

			OwnedMines = sensorData.Map.Objects
				.Where(o => o.Mine != null && o.Mine.Owner == side)
				.Select(o => o.Mine)
				.ToList();

			Scores = 0; //Начальное значение неизвестно, да и не нужно
			RespawnsCount = 0;
		}

		public readonly Dictionary<UnitType, Int32> Army;

		public readonly Dictionary<Resource, Int32> Treasury;

		public readonly List<HoMM.ClientClasses.Mine> OwnedMines;

		public Int32 Scores;

		public Int32 RespawnsCount;
	}
}