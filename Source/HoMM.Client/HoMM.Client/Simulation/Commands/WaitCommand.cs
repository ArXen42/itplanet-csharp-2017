﻿using System;
using HoMM.ClientClasses;

namespace HoMM.Client.Simulation.Commands
{
	public class WaitCommand : SimulatorCommand
	{
		public WaitCommand(WorldSimulator simulator, String side, Double targetDuration) : base(simulator, side)
		{
			_targetDuration = targetDuration;
		}

		public override HommSensorData DoInRealGame(HommClient client, HommSensorData sensorData)
		{
			return client.Wait(_targetDuration);
		}

		protected override void OnApply()
		{
			Duration = _targetDuration;
		}

		protected override void OnRollback() { }

		private readonly Double _targetDuration;
	}
}