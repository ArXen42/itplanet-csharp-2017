﻿using System;
using HoMM.ClientClasses;

namespace HoMM.Client.Simulation.Commands
{
	public abstract class SimulatorCommand
	{
		protected SimulatorCommand(WorldSimulator simulator, String side)
		{
			Simulator = simulator;
			Side = side;
			Player = Simulator.Players[side];
		}

		public readonly WorldSimulator Simulator;

		public readonly String Side;

		public SimulatorCommand ChainedCommand { get; protected internal set; }

		public Double EndTime { get; private set; }

		public Double StartTime => EndTime - Duration;

		public Double Duration
		{
			get { return _duration; }
			protected set
			{
				_duration = value;

				Double startTime = Simulator.GetLatestCommandTime(Side);
				EndTime = startTime + Duration;
			}
		}

		public abstract HommSensorData DoInRealGame(HommClient client, HommSensorData sensorData);

		public void AddCommandToChain(SimulatorCommand command)
		{
			var currentCommand = this;
			while (currentCommand.ChainedCommand != null)
				currentCommand = currentCommand.ChainedCommand;

			currentCommand.ChainedCommand = command;
		}

		internal void Apply()
		{
			OnApply();
			Scores = Player.Scores;
		}

		internal void Rollback()
		{
			OnRollback();
		}

		protected abstract void OnApply();

		protected abstract void OnRollback();

		protected readonly PlayerData Player;
		private Double _duration;
		public Int32 Scores { get; private set; }
	}
}