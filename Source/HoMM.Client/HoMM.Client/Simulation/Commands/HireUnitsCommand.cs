﻿using System;
using HoMM.ClientClasses;
using Shouldly;

namespace HoMM.Client.Simulation.Commands
{
	public sealed class HireUnitsCommand : SimulatorCommand
	{
		public HireUnitsCommand(WorldSimulator simulator, String side, Int32 count) : base(simulator, side)
		{
			_count = count;
		}

		public override HommSensorData DoInRealGame(HommClient client, HommSensorData sensorData)
		{
			return client.HireUnits(_count);
		}

		protected override void OnApply()
		{
			Duration = HommRules.Current.UnitsHireDuration;

			_dwelling = Simulator.GetPlayerNode(Side).MapObjectData.Dwelling;
			_dwelling.AvailableToBuyCount.ShouldBeGreaterThanOrEqualTo(_count);

			var playerTreasury = Player.Treasury;
			var playerArmy = Player.Army;

			_dwelling.AvailableToBuyCount -= _count;
			_dwelling.AvailableToBuyCount.ShouldBeGreaterThanOrEqualTo(0);
			playerArmy[_dwelling.UnitType] += _count;

			foreach (var resource in UnitsConstants.Current.UnitCost[_dwelling.UnitType])
			{
				playerTreasury[resource.Key] -= resource.Value*_count;
#if DEBUG
				playerTreasury[resource.Key].ShouldBeGreaterThanOrEqualTo(0);
#endif
			}

			Player.Scores += UnitsConstants.Current.Scores[_dwelling.UnitType]*_count;
		}

		protected override void OnRollback()
		{
			var playerTreasury = Player.Treasury;
			var playerArmy = Player.Army;

			_dwelling.AvailableToBuyCount += _count;
			playerArmy[_dwelling.UnitType] -= _count;

			foreach (var resource in UnitsConstants.Current.UnitCost[_dwelling.UnitType])
				playerTreasury[resource.Key] += resource.Value*_count;

			Player.Scores -= UnitsConstants.Current.Scores[_dwelling.UnitType]*_count;
		}

		private readonly Int32 _count;
		private ClientClasses.Dwelling _dwelling;
	}
}