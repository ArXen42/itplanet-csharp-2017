﻿using System;
using System.Linq;
using HoMM.ClientClasses;
using Shouldly;

namespace HoMM.Client.Simulation.Commands
{
	public sealed class RespawnSimulatorCommand : SimulatorCommand
	{
		public RespawnSimulatorCommand(WorldSimulator simulator, String side, Hero deadHero) : base(simulator, side)
		{
			_deadHero = deadHero;
		}

		public override HommSensorData DoInRealGame(HommClient client, HommSensorData sensorData)
		{
			Console.WriteLine("It seems like AIController has committed suicide.");
			return sensorData;
		}

		protected override void OnApply()
		{
			Duration = HommRules.Current.RespawnInterval;
			var respawnLocationSite = Side == "Left" ? Simulator.Map.LeftRespawnSite : Simulator.Map.RightRespawnSite;

#if DEBUG
			respawnLocationSite.MapObjectData.Hero.ShouldBeNull("We have some problems managing two heroes at one tile.");
#endif
			respawnLocationSite.MapObjectData.Hero = _deadHero;
			Player.RespawnsCount++;
		}

		protected override void OnRollback()
		{
			var respawnLocationSite = Side == "Left" ? Simulator.Map.LeftRespawnSite : Simulator.Map.RightRespawnSite;

			respawnLocationSite.MapObjectData.Hero.ShouldNotBeNull();
			respawnLocationSite.MapObjectData.Hero = null;
			Player.RespawnsCount--;
		}

		private readonly Hero _deadHero;
	}
}