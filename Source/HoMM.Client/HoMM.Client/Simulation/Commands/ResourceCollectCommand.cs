﻿using System;
using HoMM.ClientClasses;

namespace HoMM.Client.Simulation.Commands
{
	/// <summary>
	///    Resource collection command. Only for chaining with MoveCommand.
	/// </summary>
	public sealed class ResourceCollectSimulatorCommand : SimulatorCommand
	{
		public ResourceCollectSimulatorCommand(WorldSimulator simulator, String side) : base(simulator, side) { }

		public override HommSensorData DoInRealGame(HommClient client, HommSensorData sensorData)
		{
			return sensorData;
		}

		protected override void OnApply()
		{
			Duration = 0;

			var playerNode = Simulator.GetPlayerNode(Side);
			_resourcePile = playerNode.MapObjectData.ResourcePile;

			playerNode.MapObjectData.ResourcePile = null;
			Player.Treasury[_resourcePile.Resource] += _resourcePile.Amount;
			Player.Scores += HommRules.Current.ResourcesGainScores;
		}

		protected override void OnRollback()
		{
			Simulator.GetPlayerNode(Side).MapObjectData.ResourcePile = _resourcePile;
			Player.Treasury[_resourcePile.Resource] -= _resourcePile.Amount;
			Player.Scores -= HommRules.Current.ResourcesGainScores;
		}

		private ClientClasses.ResourcePile _resourcePile;
	}
}