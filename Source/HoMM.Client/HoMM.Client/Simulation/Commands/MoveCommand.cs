﻿using System;
using System.Linq;
using HoMM.ClientClasses;

namespace HoMM.Client.Simulation.Commands
{
	/// <summary>
	///    Player movement command.
	/// </summary>
	public class MoveCommand : SimulatorCommand
	{
		private const Int32 ExplorationBonus = 1;

		public MoveCommand(WorldSimulator simulator, String side, Direction direction) : base(simulator, side)
		{
			Direction = direction;
		}

		public readonly Direction Direction;

		public SimulatorMapNode From => _from;
		public SimulatorMapNode To => _to;

		public override HommSensorData DoInRealGame(HommClient client, HommSensorData sensorData)
		{
			if (sensorData.Location.X != _from.MapObjectData.Location.X || sensorData.Location.Y != _from.MapObjectData.Location.Y)
				throw new MoveCommandActException(sensorData.Location, _from.MapObjectData.Location);

			return client.Move(Direction);
		}

		protected override void OnApply()
		{
			var playerNode = Simulator.GetPlayerNode(Side);
			var edges = Simulator.Map.AdjacentEdges(playerNode);
			var targetEdge = edges.First(e => (e.Source == playerNode ? e.SourceTargetDirection : e.TargetSourceDirection) == Direction);

			Double travelCost = playerNode.MapObjectData.Terrain.ToTileTerrain().TravelCost;
			Duration = HommRules.Current.MovementDuration*travelCost;

			_from = playerNode;
			_to = targetEdge.OppositeNode(playerNode);

			var neutrals = _to.MapObjectData.NeutralArmy;
			var garrison = _to.MapObjectData.Garrison;
			if (neutrals != null || garrison != null)
			{
				Duration += HommRules.Current.CombatDuration;

				if (neutrals != null)
					AddCommandToChain(new FightWithNeutralArmyCommand(Simulator, Side));
				else
					AddCommandToChain(new FightWithGarrisonCommand(Simulator, Side));
			}
			else
			{
				if (_to.MapObjectData.ResourcePile != null)
					AddCommandToChain(new ResourceCollectSimulatorCommand(Simulator, Side));
				if (_to.MapObjectData.Mine != null && _to.MapObjectData.Mine.Owner != Side)
					AddCommandToChain(new CaptureMineCommand(Simulator, Side));
			}
			if (_to.MapObjectData.Hero != null)
				throw new NotImplementedException();

			var dwelling = _to.MapObjectData.Dwelling;
			if (dwelling != null)
			{
				dwelling.Owner = Side;
				_dwellingPreviousOwner = dwelling.Owner;

				if (_dwellingPreviousOwner != Side)
					Player.Scores += ExplorationBonus;
			}

			_to.MapObjectData.Hero = _from.MapObjectData.Hero;
			_from.MapObjectData.Hero = null;
		}

		protected override void OnRollback()
		{
			_from.MapObjectData.Hero = _to.MapObjectData.Hero;
			_to.MapObjectData.Hero = null;

			var dwelling = _to.MapObjectData.Dwelling;
			if (dwelling != null)
			{
				dwelling.Owner = _dwellingPreviousOwner;

				if (_dwellingPreviousOwner != Side)
					Player.Scores -= ExplorationBonus;
			}
		}

		private SimulatorMapNode _from;
		private SimulatorMapNode _to;

		private String _dwellingPreviousOwner;
	}

	public class MoveCommandActException : Exception
	{
		public MoveCommandActException(LocationInfo realLocation, LocationInfo expectedLocation) : base(
			$"Player location was {realLocation} but expected {expectedLocation}")
		{
			RealLocation = realLocation;
			ExpectedLocation = expectedLocation;
		}

		public readonly LocationInfo RealLocation;
		public readonly LocationInfo ExpectedLocation;
	}
}