﻿using System;
using HoMM.ClientClasses;
using Shouldly;

namespace HoMM.Client.Simulation.Commands
{
	/// <summary>
	///    Mine capture command. Only for chaining with MoveCommand.
	/// </summary>
	public class CaptureMineCommand : SimulatorCommand
	{
		private const Int32 GoldBonus = 4;
		private const Int32 EnemyMineCaptureBonus = 4;
		private const Int32 ExplorationBonus = 2;

		public CaptureMineCommand(WorldSimulator simulator, String side) : base(simulator, side) { }

		public override HommSensorData DoInRealGame(HommClient client, HommSensorData sensorData)
		{
			return sensorData;
		}

		protected override void OnApply()
		{
			Duration = 0;

			var mine = Simulator.GetPlayerNode(Side).MapObjectData.Mine;
			_previousOwner = mine.Owner;
			_previousOwner.ShouldNotBe(Side, "AIController attemped to capture mine from itself.");

			mine.Owner = Side;
			Player.OwnedMines.Add(mine);

			if (_previousOwner == Helpers.GetOppositeSide(Side))
				Player.Scores += EnemyMineCaptureBonus; //Small stealing bonus (will probably make game more agressive)

			if (_previousOwner == null)
				Player.Scores += ExplorationBonus; //To give hero that feeling of brave new world awaiting

			if (mine.Resource == Resource.Gold) //Caused because of usual lack of gold (that should've been inferred automatically by the AI...)
				Player.Scores += GoldBonus;
		}

		protected override void OnRollback()
		{
			var mine = Simulator.GetPlayerNode(Side).MapObjectData.Mine;

			mine.Owner = _previousOwner;
			Player.OwnedMines.Remove(mine);

			if (_previousOwner == Helpers.GetOppositeSide(Side))
				Player.Scores -= EnemyMineCaptureBonus;

			if (_previousOwner == null)
				Player.Scores -= ExplorationBonus;

			if (mine.Resource == Resource.Gold)
				Player.Scores -= GoldBonus;
		}

		private String _previousOwner;
	}
}