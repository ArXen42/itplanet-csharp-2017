﻿using System;
using System.Collections.Generic;
using HoMM.ClientClasses;

namespace HoMM.Client.Simulation.Commands
{
	public sealed class FightWithNeutralArmyCommand : SimulatorCommand
	{
		public FightWithNeutralArmyCommand(WorldSimulator simulator, String side) : base(simulator, side) { }

		public override HommSensorData DoInRealGame(HommClient client, HommSensorData sensorData)
		{
			return sensorData;
		}

		// At this point hero locates at the same tile as NeutralArmy
		protected override void OnApply()
		{
			Duration = 0;

			_node = Simulator.GetPlayerNode(Side);
			_hero = _node.MapObjectData.Hero;
			_neutralArmy = _node.MapObjectData.NeutralArmy;

			var armyPair = new ArmiesPair(Player.Army, _neutralArmy.Army);
			_combatResult = Combat.Resolve(armyPair);

			_attackerDelta = Helpers.GetArmiesDelta(armyPair.AttackingArmy, _combatResult.AttackingArmy);
			Helpers.AddToArmy(Player.Army, _attackerDelta);


			if (_combatResult.IsAttackerWin == _combatResult.IsDefenderWin)
			{
				_node.MapObjectData.Hero = null;
				_node.MapObjectData.NeutralArmy = null;
				AddCommandToChain(new RespawnSimulatorCommand(Simulator, Side, _hero));
			}
			else if (_combatResult.IsAttackerWin)
			{
				_node.MapObjectData.NeutralArmy = null;

				foreach (var unitPair in _neutralArmy.Army)
					Player.Scores += UnitsConstants.Current.Scores[unitPair.Key]*unitPair.Value;

				if (_node.MapObjectData.ResourcePile != null)
					AddCommandToChain(new ResourceCollectSimulatorCommand(Simulator, Side));
				if (_node.MapObjectData.Mine != null)
					AddCommandToChain(new CaptureMineCommand(Simulator, Side));
			}
			else if (_combatResult.IsDefenderWin)
			{
				_defenderDelta = Helpers.GetArmiesDelta(armyPair.DefendingArmy, _combatResult.DefendingArmy);
				Helpers.AddToArmy(_neutralArmy.Army, _defenderDelta);

				_node.MapObjectData.Hero = null;
				AddCommandToChain(new RespawnSimulatorCommand(Simulator, Side, _hero));
			}
		}

		protected override void OnRollback()
		{
			Helpers.DeductFromArmy(Player.Army, _attackerDelta);

			if (_combatResult.IsAttackerWin)
				foreach (var unitPair in _neutralArmy.Army)
					Player.Scores -= UnitsConstants.Current.Scores[unitPair.Key]*unitPair.Value;

			if (_combatResult.IsDefenderWin)
				Helpers.DeductFromArmy(_neutralArmy.Army, _defenderDelta);

			_node.MapObjectData.Hero = _hero;
			_node.MapObjectData.NeutralArmy = _neutralArmy;
		}

		private ClientClasses.NeutralArmy _neutralArmy;
		private ClientClasses.Hero _hero;
		private SimulatorMapNode _node;

		private Combat.CombatResult _combatResult;
		private Dictionary<UnitType, Int32> _attackerDelta;
		private Dictionary<UnitType, Int32> _defenderDelta;
	}
}