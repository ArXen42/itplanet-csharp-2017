﻿using System;
using System.Collections.Generic;
using HoMM.ClientClasses;

namespace HoMM.Client.Simulation
{
	public sealed class SimulatorMapNode
	{
		public SimulatorMapNode(MapObjectData mapObjectData, Boolean reachable)
		{
			MapObjectData = mapObjectData;
			Reachable = reachable;

			ShortestPathLength = new Dictionary<SimulatorMapNode, Int32>();
			PreviousNodeInPath = new Dictionary<SimulatorMapNode, SimulatorMapNode>();
		}

		public SimulatorMapNode(MapObjectData mapObjectData) : this(mapObjectData, true) { }

		public Boolean Reachable;

		public MapObjectData MapObjectData { get; }

		public readonly Dictionary<SimulatorMapNode, Int32> ShortestPathLength;
		public readonly Dictionary<SimulatorMapNode, SimulatorMapNode> PreviousNodeInPath;
	}
}