﻿using System;
using System.Diagnostics;
using System.Linq;
using HoMM.ClientClasses;
using QuickGraph;

namespace HoMM.Client.Simulation
{
	public class Map : UndirectedGraph<SimulatorMapNode, SimulatorMapEdge>
	{
		public Map(MapData mapData) : base(false)
		{
			Width = mapData.Width;
			Height = mapData.Height;

			AddVertexRange(mapData.Objects.Where(o => o.Wall == null).Select(o => new SimulatorMapNode(o)));
			var directions = new[] {Direction.Down, Direction.RightDown, Direction.RightUp, Direction.LeftDown, Direction.LeftUp, Direction.Up};

			foreach (var node in Vertices)
			{
				var neighbours = directions
					.Select(dir => new Tuple<Direction, SimulatorMapNode>(
						dir,
						GetObjectAt(node.MapObjectData.Location.ToLocation().NeighborAt(dir))))
					.Where(o => o.Item2 != null)
					.ToArray();

				foreach (var neighbour in neighbours)
				{
					if (!ContainsEdge(neighbour.Item2, node))
						AddEdge(new SimulatorMapEdge(node, neighbour.Item2, neighbour.Item1));
				}
			}

			LeftRespawnSite = GetObjectAt(Location.Zero);
			RightRespawnSite = GetObjectAt(Location.Max(new MapSize(Width, Height)));

			CalculateShorestPaths();
		}

		public readonly Int32 Width, Height;
		public readonly SimulatorMapNode LeftRespawnSite;
		public readonly SimulatorMapNode RightRespawnSite;

		public SimulatorMapNode GetObjectAt(Location location)
		{
			if (location.X < 0 || location.X >= Width || location.Y < 0 || location.Y >= Height)
				return null;

			return Vertices.FirstOrDefault(x => x.MapObjectData.Location.X == location.X && x.MapObjectData.Location.Y == location.Y);
		}

		public SimulatorMapNode GetObjectAt(LocationInfo location)
		{
			if (location.X < 0 || location.X >= Width || location.Y < 0 || location.Y >= Height)
				return null;

			return Vertices.FirstOrDefault(x => x.MapObjectData.Location.X == location.X && x.MapObjectData.Location.Y == location.Y);
		}

		private void CalculateShorestPaths()
		{
			foreach (var sourceNode in Vertices)
				CalculateShortestPath(sourceNode);
		}

		private void CalculateShortestPath(SimulatorMapNode sourceNode)
		{
			foreach (var node in Vertices)
			{
				node.ShortestPathLength.Add(sourceNode, Int32.MaxValue);
				node.PreviousNodeInPath.Add(sourceNode, null);
			}

			sourceNode.ShortestPathLength[sourceNode] = 0;

			var unvisitedNodes = Vertices.Where(node => node.Reachable).ToList();

			while (unvisitedNodes.Count > 0)
			{
				SimulatorMapNode currentNode = null;
				Int32 lowestLength = Int32.MaxValue;
				foreach (var node in unvisitedNodes) //Searching nearest unvisited node
				{
					Int32 length = node.ShortestPathLength[sourceNode];
					if (length < lowestLength)
					{
						currentNode = node;
						lowestLength = length;
					}
				}

				unvisitedNodes.Remove(currentNode);

				var neighbours = AdjacentEdges(currentNode)
					.Select(e => e.OppositeNode(currentNode))
					.Where(node => node.Reachable)
					.ToList();

				if (neighbours.Count == 0)
					Console.WriteLine("WTF");

				foreach (var neighbour in neighbours)
				{
					Int32 dist = currentNode.ShortestPathLength[sourceNode] + 1;
					if (dist < neighbour.ShortestPathLength[sourceNode])
					{
						neighbour.ShortestPathLength[sourceNode] = dist;
						neighbour.PreviousNodeInPath[sourceNode] = currentNode;
					}
				}
			}
		}
	}
}