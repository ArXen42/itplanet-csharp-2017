﻿using System;
using System.Collections.Generic;
using System.Linq;
using HoMM.Client.AI;
using HoMM.Client.Simulation.Commands;
using HoMM.ClientClasses;

namespace HoMM.Client
{
	internal class Program
	{
		public static readonly Guid CvarcTag = Guid.Parse("96d01bb7-90d8-4986-bf09-b3c4eaa71e36");

		public static void Main(String[] args)
		{
			if (args.Length == 0)
//				args = new[] {"192.168.0.71", "18700"};
//				args = new[] {"192.168.0.70", "18700"};
//				args = new[] {"127.0.0.1", "18700"};
				args = new[] {"homm.ulearn.me", "18700"};

			String ip = args[0];
			Int32 port = Int32.Parse(args[1]);

			var client = new HommClient();

			client.OnSensorDataReceived += Print;
			client.OnInfo += OnInfo;

			Console.CancelKeyPress += (sender, eventArgs) => client.Exit();

			var sensorData = client.Configurate(
				ip, port, CvarcTag,
				timeLimit: 90,
				operationalTimeLimit: 5,
				seed: 4,
				spectacularView: true,
				debugMap: false,
				level: HommLevel.Level3,
				isOnLeftSide: true
			);

			try
			{
				RunAI(client, sensorData);
			}
			catch (Exception e)
			{
				client.Exit();
				throw;
			}
		}

		private static void RunAI(HommClient client, HommSensorData sensorData)
		{
			var aiController = new AIController(sensorData);

			while (true)
			{
				var commands = aiController.GetBestSequence();

				if (commands.Count == 0)
					break;

				for (Int32 i = 0; i < commands.Count*Math.Pow((Double) aiController.RevealedObjectsCount/aiController.ObjectsOnMapCount, 1.5); i++)
				{
					if (aiController.IsPendingRecalculate)
					{
						Console.ForegroundColor = ConsoleColor.Yellow;
						Console.WriteLine("One of our mines has been lost. Recalculation initiated.");
						Console.ResetColor();
						break;
					}

					var command = commands[i];

					var commandAsMove = command as MoveCommand;
					if (commandAsMove != null)
					{
						var locationTowards = sensorData.Location.ToLocation().NeighborAt(commandAsMove.Direction);
						var neutralArmyTowards = Helpers.GetObjectAt(sensorData.Map, locationTowards).NeutralArmy;

						if (neutralArmyTowards != null)
						{
							var combatResult = Combat.Resolve(new ArmiesPair(sensorData.MyArmy, neutralArmyTowards.Army));
							if (!combatResult.IsAttackerWin || combatResult.AttackingArmy.Sum(u => u.Value) == 0)
							{
								Console.ForegroundColor = ConsoleColor.Yellow;
								Console.WriteLine("Something went wrong and AI was going to suicide. Recalculating.");
								Console.ResetColor();
								break;
							}
						}
					}

					try
					{
						sensorData = command.DoInRealGame(client, sensorData);
					}
					catch (MoveCommandActException e)
					{
						aiController.Update(sensorData, e.ExpectedLocation);

						Console.ForegroundColor = ConsoleColor.Yellow;
						Console.WriteLine($"Can't reach {e.ExpectedLocation}. Marking it unreachable.");
						Console.ResetColor();

						break;
					}

					aiController.Update(sensorData);

					if (sensorData.IsDead)
					{
						//The death site probably won't be seen from respawn location. However, we can prognose situation on that tile.
						//The problem is, we don't know if it is planned by AI or it was external interruption.
						//As for now, I will only fix the player presence on death site.

						Console.ForegroundColor = ConsoleColor.Red;
						Console.WriteLine("Death!");
						Console.ResetColor();

						Helpers.GetObjectAt(aiController.SensorData.Map, sensorData.Location).Hero = null;
						sensorData = client.Wait(HommRules.Current.RespawnInterval);
						aiController.Update(sensorData);
						break;
					}
				}
			}
		}

		private static void Print(HommSensorData data)
		{
			Console.WriteLine($"The time is {data.WorldCurrentTime}");
		}

		private static void OnInfo(String infoMessage)
		{
			Console.ForegroundColor = ConsoleColor.Green;
			Console.WriteLine(infoMessage);
			Console.ResetColor();
		}

		private static void ControlFromKeyboard(HommClient client)
		{
			Console.WriteLine("input");
			var acts = new Dictionary<ConsoleKey, Action>
			{
				{ConsoleKey.Q, () => client.Move(Direction.LeftUp)},
				{ConsoleKey.A, () => client.Move(Direction.LeftDown)},
				{ConsoleKey.W, () => client.Move(Direction.Up)},
				{ConsoleKey.S, () => client.Move(Direction.Down)},
				{ConsoleKey.E, () => client.Move(Direction.RightUp)},
				{ConsoleKey.D, () => client.Move(Direction.RightDown)},
				{ConsoleKey.Spacebar, () => client.HireUnits(20)}
			};

			acts[Console.ReadKey().Key].Invoke();
		}
	}
}