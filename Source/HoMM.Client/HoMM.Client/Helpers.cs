﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using CVARC.V2;
using HoMM.Client.Simulation;
using HoMM.ClientClasses;
using HoMM.World;
using Infrastructure;
using Newtonsoft.Json.Linq;
using Shouldly;

namespace HoMM.Client
{
	public static class Helpers
	{
		public static TileTerrain ToTileTerrain(this Terrain terrain)
		{
			switch (terrain)
			{
				case Terrain.Grass:
					return TileTerrain.Grass;
				case Terrain.Snow:
					return TileTerrain.Snow;
				case Terrain.Desert:
					return TileTerrain.Desert;
				case Terrain.Marsh:
					return TileTerrain.Marsh;
				case Terrain.Road:
					return TileTerrain.Road;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		public static MapObjectData GetObjectAt(MapData map, Location location)
		{
			if (location.X < 0 || location.X >= map.Width || location.Y < 0 || location.Y >= map.Height)
				return null;

			return map.Objects.Find(x => x.Location.X == location.X && x.Location.Y == location.Y);
		}

		public static MapObjectData GetObjectAt(MapData map, LocationInfo location)
		{
			if (location.X < 0 || location.X >= map.Width || location.Y < 0 || location.Y >= map.Height)
				return null;

			return map.Objects.Find(x => x.Location.X == location.X && x.Location.Y == location.Y);
		}

		public static HommSensorData GetClone(this HommSensorData sensorData)
		{
			return JObject.FromObject(sensorData).ToObject<HommSensorData>();
		}

		public static String GetOppositeSide(String side)
		{
			side.ShouldBeOneOf("Left", "Right");

			return side == "Left" ? "Right" : "Left";
		}

		public static Direction GetOppositeDirection(Direction direction)
		{
			switch (direction)
			{
				case Direction.Up:
					return Direction.Down;
				case Direction.Down:
					return Direction.Up;
				case Direction.LeftUp:
					return Direction.RightDown;
				case Direction.LeftDown:
					return Direction.RightUp;
				case Direction.RightUp:
					return Direction.LeftDown;
				case Direction.RightDown:
					return Direction.LeftUp;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		public static SimulatorMapNode OppositeNode(this SimulatorMapEdge edge, SimulatorMapNode source)
		{
#if DEBUG
			source.ShouldBeOneOf(edge.Source, edge.Target);
#endif
			return edge.Source == source ? edge.Target : edge.Source;
		}

		public static SimulatorMapNode OppositeNode(this SimulatorMapEdge edge, SimulatorMapNode source, out Direction direction)
		{
#if DEBUG
			source.ShouldBeOneOf(edge.Source, edge.Target);
#endif
			if (edge.Source == source)
			{
				direction = edge.SourceTargetDirection;
				return edge.Target;
			}
			else
			{
				direction = edge.TargetSourceDirection;
				return edge.Source;
			}
		}


		public static Dictionary<UnitType, Int32> GetArmiesDelta(Dictionary<UnitType, Int32> army1, Dictionary<UnitType, Int32> army2)
		{
			var result = new Dictionary<UnitType, Int32>();

			foreach (var army1Pair in army1)
			{
				Int32 army2Value;
				army2.TryGetValue(army1Pair.Key, out army2Value);
				result.Add(army1Pair.Key, army2Value - army1Pair.Value);
			}

			return result;
		}

		public static Dictionary<Resource, Int32> GetResourcesDelta(Dictionary<Resource, Int32> treasury1, Dictionary<Resource, Int32> treasury2)
		{
			return new Dictionary<Resource, Int32>
			{
				{Resource.Gold, treasury2[Resource.Gold] - treasury1[Resource.Gold]},
				{Resource.Ebony, treasury2[Resource.Ebony] - treasury1[Resource.Ebony]},
				{Resource.Glass, treasury2[Resource.Glass] - treasury1[Resource.Glass]},
				{Resource.Iron, treasury2[Resource.Iron] - treasury1[Resource.Iron]}
			};
		}

		public static void AddToArmy(Dictionary<UnitType, Int32> army, Dictionary<UnitType, Int32> delta)
		{
			foreach (var pair in delta)
				army[pair.Key] += pair.Value;
		}

		public static void DeductFromArmy(Dictionary<UnitType, Int32> army, Dictionary<UnitType, Int32> delta)
		{
			foreach (var pair in delta)
				army[pair.Key] -= pair.Value;
		}


		public static Int32 HowMuchUnitsCanBeHired(ClientClasses.Dwelling dwelling, Dictionary<Resource, Int32> treasury)
		{
			var requiredResources = UnitsConstants.Current.UnitCost[dwelling.UnitType];

			Int32 count = requiredResources.Min(r => treasury[r.Key]/requiredResources.Count);

			return count < dwelling.AvailableToBuyCount ? count : dwelling.AvailableToBuyCount;
		}

		/// <summary>
		///    Useful for getting data for tests.
		/// </summary>
		public static JObject GetSensorDataJson(String ip, Int32 port, Guid cvarcTag, HommLevel level = HommLevel.Level1, Boolean isOnLeftSide = true,
			Int32 timeLimit = 90, Int32 operationalTimeLimit = 1000, Int32 seed = 0, Boolean speedUp = false, Boolean debugMap = false,
			Boolean spectacularView = true)
		{
			GameSettings gameSettings = new GameSettings();
			LoadingData loadingData = new LoadingData {AssemblyName = "homm"};
			String str1 = level.ToString();
			loadingData.Level = str1;
			gameSettings.LoadingData = loadingData;
			Int32 num1 = spectacularView ? 1 : 0;
			gameSettings.SpectacularView = num1 != 0;
			Int32 num2 = speedUp ? 1 : 0;
			gameSettings.SpeedUp = num2 != 0;
			Double num3 = (Double) timeLimit;
			gameSettings.TimeLimit = num3;
			Double num4 = (Double) operationalTimeLimit;
			gameSettings.OperationalTimeLimit = num4;
			var actorSettingsList = new List<ActorSettings>();
			ActorSettings actorSettings1 = new ActorSettings {ControllerId = isOnLeftSide ? "Left" : "Right"};
			Int32 num5 = 0;
			actorSettings1.IsBot = num5 != 0;
			actorSettings1.PlayerSettings = new PlayerSettings()
			{
				CvarcTag = cvarcTag
			};
			actorSettingsList.Add(actorSettings1);
			ActorSettings actorSettings2 = new ActorSettings();
			actorSettings2.ControllerId = isOnLeftSide ? "Right" : "Left";
			Int32 num6 = 1;
			actorSettings2.IsBot = num6 != 0;
			String str2 = "Standing";
			actorSettings2.BotName = str2;
			actorSettingsList.Add(actorSettings2);
			gameSettings.ActorSettings = actorSettingsList;
			GameSettings configuration = gameSettings;

			var client = new TcpClient();
			try
			{
				client.Connect(ip, port);
			}
			catch (SocketException ex)
			{
				throw new ClientException("Cant connect to server. Wrong address or proxy not started");
			}
			client.WriteJson(configuration);
			client.WriteJson(JObject.FromObject(new HommWorldState(seed, debugMap)));


			client.ReadJson<PlayerMessage>(); //greetings
			var playerMessage = client.ReadJson<PlayerMessage>(); //actual sensor data

			return playerMessage.Message;
		}
	}
}